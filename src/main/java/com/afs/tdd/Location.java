package com.afs.tdd;

public class Location {
    private int x;
    private int y;
    private Direction direction;

    public Location(int x, int y, Direction direction) {

        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setDirection(int index) {
        switch(index){
            case 0:
                this.direction = Direction.NORTH;
                break;
            case 1:
                this.direction = Direction.WEST;
                break;
            case 2:
                this.direction = Direction.SOUTH;
                break;
            case 3:
                this.direction = Direction.EAST;
                break;
        }
    }
}
