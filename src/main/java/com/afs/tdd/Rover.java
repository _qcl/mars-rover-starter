package com.afs.tdd;

import java.util.List;

public class Rover {
    private Location location;

    public Rover(Location location) {
        this.location = location;
    }

    private void roverMove(){
        switch(this.location.getDirection()){
            case NORTH:
                this.location.setY(location.getY() + 1);
                break;
            case WEST:
                this.location.setX(location.getX() - 1);
                break;
            case SOUTH:
                this.location.setY(location.getY() - 1);
                break;
            case EAST:
                this.location.setX(location.getX() + 1);
                break;
        }
    }

    private void turnLeft(){
        this.location.setDirection((this.location.getDirection().ordinal() + 1) % 4);
    }

    private void turnRight(){
        this.location.setDirection((this.location.getDirection().ordinal() + 3) % 4);
    }

    public void executeCommand(Command command) {
        if(command == Command.MOVE){
            roverMove();
        } else if (command == Command.TURNLEFT){
            turnLeft();
        } else if (command == Command.TURNRIGHT){
            turnRight();
        }
    }

    public void executeCommand(List<Command> commands) {
        commands.forEach(this::executeCommand);
    }
}
