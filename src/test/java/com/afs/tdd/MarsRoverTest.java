package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_only_plus_location_y_1_when_executeCommand_given_location_and_command_move() {
        Location location = new Location(0,0,Direction.NORTH);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.MOVE);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(1, location.getY());
        Assertions.assertSame(Direction.NORTH, location.getDirection());
    }

    @Test
    void should_only_reduce_location_x_1_when_executeCommand_given_location_and_command_move() {
        Location location = new Location(0,0,Direction.WEST);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.MOVE);

        Assertions.assertEquals(-1, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.WEST, location.getDirection());
    }
    @Test
    void should_only_reduce_location_y_1_when_executeCommand_given_location_and_command_move() {
        Location location = new Location(0,0,Direction.SOUTH);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.MOVE);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(-1, location.getY());
        Assertions.assertSame(Direction.SOUTH, location.getDirection());
    }
    @Test
    void should_only_plus_location_x_1_when_executeCommand_given_location_and_command_move() {
        Location location = new Location(0,0,Direction.EAST);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.MOVE);

        Assertions.assertEquals(1, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.EAST, location.getDirection());
    }
    @Test
    void should_change_direction_to_west_when_executeCommand_given_location_and_command_turn_left() {
        Location location = new Location(0,0,Direction.NORTH);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNLEFT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.WEST, location.getDirection());
    }
    @Test
    void should_change_direction_to_south_when_executeCommand_given_location_and_command_turn_left() {
        Location location = new Location(0,0,Direction.WEST);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNLEFT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.SOUTH, location.getDirection());
    }
    @Test
    void should_change_direction_to_east_when_executeCommand_given_location_and_command_turn_left() {
        Location location = new Location(0,0,Direction.SOUTH);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNLEFT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.EAST, location.getDirection());
    }
    @Test
    void should_change_direction_to_north_when_executeCommand_given_location_and_command_turn_left() {
        Location location = new Location(0,0,Direction.EAST);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNLEFT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.NORTH, location.getDirection());
    }
    @Test
    void should_change_direction_to_east_when_executeCommand_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.NORTH);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNRIGHT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.EAST, location.getDirection());
    }
    @Test
    void should_change_direction_to_south_when_executeCommand_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.EAST);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNRIGHT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.SOUTH, location.getDirection());
    }
    @Test
    void should_change_direction_to_west_when_executeCommand_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.SOUTH);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNRIGHT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.WEST, location.getDirection());
    }
    @Test
    void should_change_direction_to_north_when_executeCommand_given_location_and_command_turn_right() {
        Location location = new Location(0,0,Direction.WEST);
        Rover rover = new Rover(location);
        rover.executeCommand(Command.TURNRIGHT);

        Assertions.assertEquals(0, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.NORTH, location.getDirection());
    }
    @Test
    void should_change_direction_to_east_and_plus_location_x_2_when_executeCommand_given_location_and_command_list() {
        Location location = new Location(0,0,Direction.NORTH);
        Rover rover = new Rover(location);
        List<Command> commands = Arrays.asList(Command.TURNRIGHT, Command.MOVE, Command.MOVE);
        rover.executeCommand(commands);

        Assertions.assertEquals(2, location.getX());
        Assertions.assertEquals(0, location.getY());
        Assertions.assertSame(Direction.EAST, location.getDirection());
    }
}
